# Open Orchard

Open Orchard is an Android application that maps out forageable plants around you using data from [Falling Fruit](https://fallingfruit.org/about?c=forager%2Cfreegan&locale=en). This is a remake of the [existing mobile application](https://github.com/falling-fruit/falling-fruit-mobile) but written in Java instead of Cordova.


When foraging please follow the [Code of Conduct](https://docs.google.com/document/d/1SupIGQKC5Vgi3VYkdIQc05y_S7jSoZ4RTS-CzwlEAMY/edit).
## Installation

Open in [Android Studio](https://developer.android.com/studio/). 

Create an API following [these instructions](https://developers.google.com/maps/documentation/android-sdk/start#get-key).

Add the API key to the local.properties file so you should have a line like this:

MAPS_API_KEY=AIz*******************

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Authors and acknowledgment
Ethan Welty, Jeff Wanner, Craig Durkin, Emily Sigman, Alan Gibson, Ana Carolina de Lima, Caleb Phillips, Cristina Rubke, David Craft, and Tristram Stuart for the creation of Falling Fruit.


[Ethan Welty](https://github.com/ezwelty), [Rylan Bowers](https://github.com/rylanb), [somerandomsequence](https://github.com/somerandomsequence), [Bion Johnson](https://github.com/bion), and [
David Balatero](https://github.com/dbalatero) for their work on the original application

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
