package com.example.openorchard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

public class GMap implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnCameraIdleListener
{
	GoogleMap map;
	Activity app;

	public Location lastKnownLocation;

	private Marker lastOpened;

	private List<MapEvents> listeners;
	private SupportMapFragment mapFragment;

	private List<Marker> markers;

	private ClusterManager<ForageMarker> clusterManager;
	private Context context;
	boolean debug;

	public GMap(Activity activity, SupportMapFragment fragment, Context appContext)
	{
		app = activity;
		mapFragment = fragment;
		context = appContext;
		markers = new ArrayList<>();
		listeners = new ArrayList<>();
		debug = true;
	}

	public void AddListener(MapEvents listener)
	{
		listeners.add(listener);
	}

	@Override
	public void onMapLoaded()
	{
		if (map != null)
		{
			SetMapCorners();
		}
	}

	/**
	 * Manipulates the map when it's available.
	 * This callback is triggered when the map is ready to be used.
	 */
	@SuppressLint("MissingPermission")
	@Override
	public void onMapReady(GoogleMap m)
	{
		map = m;

		clusterManager = new ClusterManager<>(context, map);

		clusterManager.setRenderer(new CustomMapClusterRenderer<>(context, map, clusterManager));

		map.setOnMapLoadedCallback(this);
		map.setOnCameraIdleListener(clusterManager);

//		map.setOnMapLoadedCallback(this);
//		map.setOnCameraIdleListener(this);

		// Use a custom info window adapter to handle multiple lines of text in the
		// info window contents.
		map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter()
		{
			@Override
			// Return null here, so that getInfoContents() is called next.
			public View getInfoWindow(Marker arg0)
			{
				return null;
			}

			@Override
			public View getInfoContents(Marker marker)
			{
				// Inflate the layouts for the info window, title and snippet.
				View infoWindow = app.getLayoutInflater().inflate(R.layout.custom_info_contents,
						app.findViewById(R.id.map), false);

				TextView title = infoWindow.findViewById(R.id.title);
				title.setText(marker.getTitle());

				TextView snippet = infoWindow.findViewById(R.id.snippet);
				snippet.setText(marker.getSnippet());

				return infoWindow;
			}
		});

		this.map.setOnMarkerClickListener(marker ->
		{
			// Check if there is an open info window
			if (lastOpened != null)
			{
				// Close the info window
				lastOpened.hideInfoWindow();

				// Is the marker the same marker that was already open
				if (lastOpened.equals(marker))
				{
					// Nullify the lastOpened object
					lastOpened = null;
					// Return so that the info window isn't opened again
					return true;
				}
			}

			// Open the info window for the marker
			marker.showInfoWindow();
			// Re-assign the last opened such that we can close it later
			lastOpened = marker;

			// Event was handled by our code do not launch default behaviour.
			return true;
		});

		map.setMyLocationEnabled(true);
		map.getUiSettings().setMyLocationButtonEnabled(true);

		for (MapEvents l : listeners)
		{
			l.MapLoaded();
		}
	}

	@Override
	public void onCameraIdle()
	{
		if (map == null || mapFragment.getView().getRight() < 1)
		{
			return;
		}

		SetMapCorners();

//		Toast.makeText(this, "The camera has stopped moving.",
//				Toast.LENGTH_SHORT).show();
	}

	private void SetMapCorners()
	{
		if (!debug)
		{
			if (map == null || mapFragment.getView().getRight() < 1 || map.getCameraPosition().zoom < 13.0f)
			{
				return;
			}
		}

		Projection mapProjection = map.getProjection();

		LatLngBounds viewBounds = mapProjection.getVisibleRegion().latLngBounds;

		for (MapEvents l : listeners)
		{
			l.SetCorners(viewBounds);
		}

		debug = false;
	}

	public void CreateMarker(String title, LatLng location, Boolean filtered)
	{
		ForageMarker fm = new ForageMarker(location.latitude, location.longitude, title, "");

		clusterManager.addItem(fm);

		//		Marker m = map.addMarker((new MarkerOptions()
//				.title(title + markers.size())
//				.position(location)
//				.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_dot))
//				.visible(!filtered)
//		));
//		markers.add(m);
	}

	public void ClearMarkers()
	{
		for (int a = 0; a < markers.size(); ++a)
		{
			markers.get(a).remove();
		}

		markers.clear();
	}

	public void FindMarkers(String filter)
	{
		boolean isFilterNullOrEmpty = filter == null || filter.trim().isEmpty();

		for (int a = 0; a < markers.size(); ++a)
		{
			String title = markers.get(a).getTitle();

			markers.get(a).setVisible(!isFilterNullOrEmpty && title.contains(filter));
		}
	}
}
