package com.example.openorchard;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class ForageMarker implements ClusterItem
{
	private final LatLng position;
	private final String title;
	private final String snippet;

	public ForageMarker(double lat, double lng, String title, String snippet)
	{
		position = new LatLng(lat, lng);
		this.title = title;
		this.snippet = snippet;
	}

	@Override
	public LatLng getPosition()
	{
		return position;
	}

	@Override
	public String getTitle()
	{
		return title;
	}

	@Override
	public String getSnippet()
	{
		return snippet;
	}
}
