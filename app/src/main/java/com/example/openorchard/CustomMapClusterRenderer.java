package com.example.openorchard;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class CustomMapClusterRenderer<T extends ClusterItem> extends DefaultClusterRenderer<T>
{
	CustomMapClusterRenderer(Context context, GoogleMap map, ClusterManager<T> clusterManager)
	{
		super(context, map, clusterManager);
	}

	@Override
	protected boolean shouldRenderAsCluster(Cluster<T> cluster)
	{
		//start clustering if more than N items overlap
		return cluster.getSize() > 3;
	}

	@Override
	protected void onBeforeClusterItemRendered(T item, MarkerOptions markerOptions)
	{
//		ForageMarker markerItem = (ForageMarker) item;
		markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_dot));
	}
}
