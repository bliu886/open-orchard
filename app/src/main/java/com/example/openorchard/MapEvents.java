package com.example.openorchard;

import com.google.android.gms.maps.model.LatLngBounds;

public interface MapEvents
{
	void MapLoaded();

	void SetCorners(LatLngBounds viewBounds);
}
