// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.example.openorchard;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * An activity that displays a map showing the place at the device's current location.
 */
public class OpenOrchard extends AppCompatActivity implements MapEvents
{
	private static final String AppName = OpenOrchard.class.getSimpleName();

	// Keys for storing activity state.
	private static final String KEY_LOCATION = "location";

	// The entry point to the Places API.
	private PlacesClient placesClient;

	private final String fruitAPI = "http://fallingfruit.org/locations/data.csv?muni=1&nelat={0}&nelng={1}&swlat={2}&swlng={3}&locale=en";

	private AndroidLocationPermission locationPermission;
	private AndroidLocation deviceLocation;
	private GMap gMap;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Context appContext = getApplicationContext();

		locationPermission = new AndroidLocationPermission(appContext, this);
		locationPermission.RequestLocationPermission();

		// Retrieve the content view that renders the map.
		setContentView(R.layout.activity_maps);

		// Construct a PlacesClient
		Places.initialize(appContext, BuildConfig.MAPS_API_KEY);
		placesClient = Places.createClient(this);

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

		gMap = new GMap(this, mapFragment, appContext);
		gMap.AddListener(this);

		// Retrieve location from saved instance state.
		if (savedInstanceState != null)
		{
			gMap.lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
		}

		mapFragment.getMapAsync(gMap);
	}

	/**
	 * Saves the state of the map when the activity is paused.
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		if (gMap != null)
		{
			outState.putParcelable(KEY_LOCATION, gMap.lastKnownLocation);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		locationPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (!locationPermission.locationPermissionGranted)
		{
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	private void GetFruitData(String site)
	{
		// Save server response text.
		final String[] csvFile = new String[1];
		Thread networkThread = new Thread()
		{
			@Override
			public void run()
			{
				csvFile[0] = Network.GetRequest(site);
			}
		};
		// Start the child thread to request web page.
		networkThread.start();
		try
		{
			networkThread.join();
			List<String[]> parsed = CSVParser.parse(csvFile[0].replace("\"\"", ""), 21);

			gMap.ClearMarkers();

			ParseData(parsed);
		}
		catch (InterruptedException | IOException e)
		{
		}
	}

	private void ParseData(List<String[]> csv)
	{
		int csvSize = csv.size();
		for (int a = 1; a < csvSize; ++a)
		{
			try
			{
				double lat = Double.parseDouble(csv.get(a)[1]);
				double lng = Double.parseDouble(csv.get(a)[2]);
				gMap.CreateMarker(csv.get(a)[csv.get(a).length - 1], new LatLng(lat, lng), false);
			}
			catch (Exception e)
			{
				// If can't parse data properly, ignore it.
				Toast.makeText(this, "Parse Exception", Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void MapLoaded()
	{
		deviceLocation = new AndroidLocation(locationPermission, this, gMap.map);

		// Get the current location of the device and set the position of the map.
		deviceLocation.getDeviceLocation();
	}

	@Override
	public void SetCorners(LatLngBounds viewBounds)
	{
		String site = MessageFormat.format(fruitAPI, viewBounds.northeast.latitude, viewBounds.northeast.longitude, viewBounds.southwest.latitude, viewBounds.southwest.longitude);

//		Toast.makeText(app, String.valueOf(map.getCameraPosition().zoom), Toast.LENGTH_LONG).show();

		GetFruitData(site);
	}
}
