package com.example.openorchard;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class AndroidLocationPermission
{
	public boolean locationPermissionGranted;

	private Context appContext;
	private Activity app;

	private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

	public AndroidLocationPermission(Context context, Activity activity)
	{
		appContext = context;
		app = activity;
	}

	/**
	 * Prompts the user for permission to use the device location.
	 */
	public void RequestLocationPermission()
	{
		if (ContextCompat.checkSelfPermission(appContext,
				android.Manifest.permission.ACCESS_FINE_LOCATION)
				== PackageManager.PERMISSION_GRANTED)
		{
			locationPermissionGranted = true;
		}
		else
		{
			ActivityCompat.requestPermissions(app,
					new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
					PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
		}
	}

	/**
	 * Handles the result of the request for location permissions.
	 */
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		locationPermissionGranted = false;
		if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
		{
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
			{
				locationPermissionGranted = true;
			}
		}
//		updateLocationUI();
	}
}
