package com.example.openorchard;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

//--------------------------------------------------------------------------------
// Parser for files that contain comma separated values.
// This is designed to handle EXCEL exported files.  For example
/*  ====  SAMPLE BEGIN ====
	"Name", "Age", Description
	John Henckel,52,This is a string of text
	Carin Pie,28,"This
	text is broken
	over \"several\" lines."
	Phen "Bob" Weirsby,,missing age
	====  SAMPLE END ==== */
// Notice that newline is a record delimiter, except within quotes. Also within quotes
// you can have escape sequences \n \" \\ \t. Other escape sequences are not changed.
// The " character is allowed in a non-quoted field if it is not the first character.
// Fields not in quotes are trimmed for whitespace.
public class CSVParser
{
	static int csvCharIndex = 0;
	//--------------------------------------------------------------------------------
	// Main function.  Read entire file into list of rows.
	// messages - Any warning messages are appended here.
	public static List<String[]> parse(String csv, int columns) throws IOException
	{
		csvCharIndex = 0;
		List<String[]> result = new ArrayList<>();

		if (csv == null || csv.length() < 1)
		{
			return result;
		}

		while (true)
		{
			String[] row = parseRecord(csv, columns);
			if (row == null)
			{
				break;
			}
			result.add(row);
			if (row.length > columns)
			{
				columns = Math.min(row.length, 2 * columns);
			}
		}
		return result;
	}

	//--------------------------------------------------------------------------------
	// This reads a record from a CSV file into an array of strings.
	// Returns null for EOF.
	// The hint is the expected number of fields per record.

	public static String[] parseRecord(String csv, int hint) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		hint = Math.min(Math.max(10, hint), 1000);
		List<String> row = new ArrayList<>(hint);
		for (int i = 0; i < hint; ++i)
		{
			int c = parseField(csv, sb);
			if (i == 0 && sb.length() == 0)
			{
				if (c == -1)
				{
					return null;        // end of the file
				}
				if (c == '\n')
				{
					continue;        // ignore blank lines in the middle of the file
				}
			}
			row.add(sb.toString());
			if (c != ',')
			{
				break;
			}
		}
		return row.toArray(new String[row.size()]);
	}

	//--------------------------------------------------------------------------------
	// This reads a single FIELD item from a CSV file into a string buffer
	// Returns the last character read, which is comma, newline, or EOF (-1).
	// Append any warnings to messages.

	public static int parseField(String csv, StringBuilder result) throws IOException
	{
		if (csvCharIndex == csv.length())
		{
			return -1;
		}

		String dump = null;
		result.setLength(0);
		int c = csv.charAt(csvCharIndex);
		csvCharIndex++;
		int numChars = 0;      // number of non-white chars that are added to the result
		for (; ; )
		{
			if (c == '\n' || c == ',' || c == -1)
			{
				if (dump != null)
				{
				}
				if (numChars > 0)
				{
					// trim trailing whitespace for non-quoted fields
					while (numChars > 1 && Character.isWhitespace(result.charAt(numChars - 1)))
					{
						--numChars;
					}
					result.setLength(numChars);
				}
				return c;
			}
			if (numChars == 0 && c == '"')
			{
				result.setLength(0);    // Ignore whitespace in front of quotes
				for (; ; )
				{
					if (csvCharIndex == csv.length())
					{
						c = -1;
					}
					else
					{
						c = csv.charAt(csvCharIndex);
						csvCharIndex++;
					}

					if (c == '"' || c == -1)
					{
						if (c == -1)
						{
							return c;
						}
						break;
					}
					if (c == '\\')
					{
						if (csvCharIndex == csv.length())
						{
							c = -1;
						}
						else
						{
							c = csv.charAt(csvCharIndex);
							csvCharIndex++;
						}
						// Interpret \n, \t, \\, and \" in the strings
						if (c == 'n')
						{
							c = '\n';
						}
						else if (c == 't')
						{
							c = '\t';
						}
						else if (c != '"' && c != '\\')
						{
							result.append('\\');
						}
					}
					result.append((char) c);
				}
				// The value -1 means we finished parsing the quotes and now we're looking for a comma or newline.
				numChars = -1;
			}
			else
			{
				if (numChars == -1)
				{
					if (dump != null)
					{
						dump += (char) c;
					}
					else if (!Character.isWhitespace(c))
					{
						dump = "" + (char) c;
					}
				}
				else
				{
					if (numChars > 0 || !Character.isWhitespace(c))
					{
						result.append((char) c);
						++numChars;
					}
				}
			}
			if (csvCharIndex == csv.length())
			{
				c = -1;
			}
			else
			{
				c = csv.charAt(csvCharIndex);
				csvCharIndex++;
			}
		}
	}
}
