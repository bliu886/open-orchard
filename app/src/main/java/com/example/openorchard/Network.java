package com.example.openorchard;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class Network
{
	private static boolean useSecureConnection;

	private static HttpURLConnection networkConnection;

	public static String GetRequest(String site)
	{
		useSecureConnection = site.contains("https");
		BufferedReader networkInputReader = null;
		StringBuilder result = new StringBuilder();

		try
		{
			URL url = new URL(site);

			if (useSecureConnection)
			{
				networkConnection = (HttpsURLConnection) url.openConnection();
			}
			else
			{
				networkConnection = (HttpURLConnection) url.openConnection();
			}

			networkConnection.setRequestMethod("GET");

			// Set connection timeout and read timeout value.
			networkConnection.setConnectTimeout(10000);
			networkConnection.setReadTimeout(10000);

			networkInputReader = new BufferedReader(new InputStreamReader(networkConnection.getInputStream()));

			String line = networkInputReader.readLine();

			while (line != null)
			{
				result.append(line).append('\n');
				line = networkInputReader.readLine();
			}
		}
		catch (Exception e)
		{
			Log.e("Error connecting", e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (networkInputReader != null)
				{
					networkInputReader.close();
				}

				if (networkConnection != null)
				{
					networkConnection.disconnect();
				}
			}
			catch (Exception e)
			{
				Log.e("Error Network Cleanup", e.getMessage(), e);
			}
		}

		return result.toString();
	}
}
