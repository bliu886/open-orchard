package com.example.openorchard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;

public class AndroidLocation
{
	public LatLng coordinates;

	private AndroidLocationPermission locationPermission;
	private Location lastKnownLocation;

	private final int defaultZoom = 10;
	private GoogleMap map;
	private final FusedLocationProviderClient fusedLocationProviderClient;
	private Activity app;

	private final LatLng defaultLocation = new LatLng(-33.8523341, 151.2106085);

	public AndroidLocation(AndroidLocationPermission permission, Activity activity, GoogleMap m)
	{
		locationPermission = permission;
		map = m;
		app = activity;
		fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(app);
	}

	/**
	 * Gets the current location of the device, and positions the map's camera.
	 */
	public void getDeviceLocation()
	{
		/*
		 * Get the best and most recent location of the device, which may be null in rare
		 * cases when a location is not available.
		 */
		try
		{
			if (locationPermission.locationPermissionGranted)
			{
				@SuppressLint("MissingPermission") Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();

				locationResult.addOnCompleteListener(app, task ->
				{
					if (task.isSuccessful())
					{
						// Set the map's camera position to the current location of the device.
						lastKnownLocation = task.getResult();
						if (lastKnownLocation != null)
						{
							map.moveCamera(CameraUpdateFactory.newLatLngZoom(
									new LatLng(lastKnownLocation.getLatitude(),
											lastKnownLocation.getLongitude()), defaultZoom));
						}
					}
					else
					{
						Log.d("Android Location", "Current location is null. Using defaults.");
						Log.e("Android Location", "Exception: %s", task.getException());
						map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, defaultZoom));
						map.getUiSettings().setMyLocationButtonEnabled(false);
					}
				});
			}
		}
		catch (SecurityException e)
		{
			Log.e("Exception: %s", e.getMessage(), e);
		}
	}
}
